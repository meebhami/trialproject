package mohammad.androidtest.simra.twtest_mohammad.Helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;

import org.json.JSONException;

import java.io.IOException;

import mohammad.androidtest.simra.twtest_mohammad.R;

/**
 * Created by mohammad on 1/9/18.
 */

public class ProgressAndException {
    ProgressDialog mProgressDialog;
    ProgressBar progressBar;
    Dialog progressdialog;
    Toast toast;
    private android.graphics.drawable.AnimationDrawable animationDrawable;

    public void  startProgress(Context context){
        progressdialog = new Dialog(context);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.dialog_progressbar);
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



        ImageView mProgressBar=progressdialog.findViewById(R.id.ivProgress);
        Animation anim = new RotateAnimation(-10.0f, 10.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);
        anim.setRepeatMode(Animation.REVERSE);
        mProgressBar.startAnimation(anim);



        progressdialog.setCancelable(false);
        progressdialog.show();

    }
    public void endProgress(Context context){
        if(progressdialog.isShowing()){
            progressdialog.dismiss();
        }
    }

    public void startProgress(ProgressBar progressBar) {
        this.progressBar=progressBar;
        this.progressBar.setIndeterminate(true);
        if (this.progressBar.getVisibility() != View.VISIBLE)
            this.progressBar.setVisibility(View.VISIBLE);
    }

    public void endProgress(ProgressBar progressBar) {
        if (this.progressBar.getVisibility() == View.VISIBLE) {
            this.progressBar.setVisibility(View.GONE);
        }
    }


    public void cancel(){
        try {
            toast.cancel();
        }catch (Exception ex){

        }
    }

    public void errorHandeling(Context context, Throwable t) {
        try {
            if (t instanceof java.net.SocketTimeoutException)
                toast.makeText(context, context.getString(R.string.SOCKETException), Toast.LENGTH_SHORT).show();
            else if (t instanceof JSONException)
                toast.makeText(context, context.getString(R.string.JSONExceeption), Toast.LENGTH_SHORT).show();
            else if (t instanceof JsonSyntaxException)
                toast.makeText(context, context.getString(R.string.JSONExceeption), Toast.LENGTH_SHORT).show();
            else if (t instanceof IOException)
                toast.makeText(context, context.getString(R.string.IOException), Toast.LENGTH_SHORT).show();
            else
                toast.makeText(context, context.getString(R.string.BASEError), Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
                toast.makeText(context, context.getString(R.string.BASEError), Toast.LENGTH_SHORT).show();
        }

    }


}
