package mohammad.androidtest.simra.twtest_mohammad.Activities;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.facebook.shimmer.ShimmerFrameLayout;

import org.reactivestreams.Publisher;

import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import mohammad.androidtest.simra.twtest_mohammad.Abstracts.MovieListPagination;
import mohammad.androidtest.simra.twtest_mohammad.Adapters.MoviesListAdapter;
import mohammad.androidtest.simra.twtest_mohammad.Helper.CheckNet;
import mohammad.androidtest.simra.twtest_mohammad.Helper.NetworkChecker;
import mohammad.androidtest.simra.twtest_mohammad.Helper.ProgressAndException;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListModel;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListStatus;
import mohammad.androidtest.simra.twtest_mohammad.NetworkAPI.RetrofitInstance;
import mohammad.androidtest.simra.twtest_mohammad.NetworkAPI.WebServices;
import mohammad.androidtest.simra.twtest_mohammad.R;
import retrofit2.Response;

public class MoviesListActivity extends AppCompatActivity {
    ProgressAndException progressAndException;
    ShimmerFrameLayout mShimmerViewContainer;
    private ProgressBar prb_pagination;
    MoviesListAdapter moviesListAdapter;
    LinearLayoutManager linearLayoutManagerH;
    RecyclerView rec_movieList;
    EditText edit_search;
    ImageView img_search;
    String apikey = "9af01761", s = "bad";
    int page = 1;
    DividerItemDecoration dividerItemDecoration;
    ArrayList<MoviesListModel> moviesListModels;

    public boolean requestHasCalled = false;
    private RecyclerView.LayoutManager mLayoutManager;


    private PublishProcessor<Integer> pagination;
    private CompositeDisposable compositeDisposable;

    private String TAG = "MainActivity";
    private WebServices service;
    private @Nullable
    ValueAnimator mValueAnimator;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // do nothing, just override
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_list);


        if (NetworkChecker.isOnline(this)) {
            initialize();
            event();
            getMoviesList(this, apikey, s);
        } else {
            new CheckNet().ShowDialog(this);
        }


    }

    // all field and attribute implementation define here
    private void initialize() {
        progressAndException = new ProgressAndException();
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        prb_pagination = findViewById(R.id.prb_pagination);
        rec_movieList = findViewById(R.id.rec_movieList);
        img_search = findViewById(R.id.img_search);
        edit_search = findViewById(R.id.edit_search);

        moviesListModels = new ArrayList<>();
        linearLayoutManagerH = new LinearLayoutManager(this);
        pagination = PublishProcessor.create();
        compositeDisposable = new CompositeDisposable();
        service = RetrofitInstance.getRetrofitInstance(this).create(WebServices.class);
        mLayoutManager = new LinearLayoutManager(this);
        rec_movieList.setLayoutManager(mLayoutManager);
        rec_movieList.setHasFixedSize(true);

    }

    // all events implemented here
    private void event() {
        final LinearLayoutManager layoutManager = (LinearLayoutManager) mLayoutManager;
        rec_movieList.addOnScrollListener(new MovieListPagination(layoutManager) {
            @Override
            public void onLoadMore(int currentPage, int totalItemCount, View view) {
                if (!requestHasCalled) {
                    page = ++currentPage;
                    pagination.onNext(page);
                }
            }
        });


        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edit_search.getText().toString().equals("")) {
                    if (NetworkChecker.isOnline(MoviesListActivity.this)) {
                        moviesListModels = new ArrayList<>();
                        rec_movieList.setAdapter(null);
                        page = 1;
                        hideKeyboard(MoviesListActivity.this);
                        getMoviesList(MoviesListActivity.this, apikey, edit_search.getText().toString());
                    } else {
                        new CheckNet().ShowDialog(MoviesListActivity.this);
                    }
                }
            }
        });

    }

    // prepare and emit data to adapter
    private void setAdapter(ArrayList<MoviesListModel> moviesListModels, String apikey) {

        this.moviesListModels = moviesListModels;
        dividerItemDecoration = new DividerItemDecoration(rec_movieList.getContext(), DividerItemDecoration.VERTICAL);
        rec_movieList.addItemDecoration(dividerItemDecoration);
        moviesListAdapter = new MoviesListAdapter(this, moviesListModels, apikey);
        rec_movieList.setAdapter(moviesListAdapter);
    }

    private void notifyAdapter(ArrayList<MoviesListModel> moviesListModels) {
        this.moviesListModels.addAll(moviesListModels);
        moviesListAdapter.notifyItemInserted(moviesListModels.size() - 1);
        moviesListAdapter.notifyDataSetChanged();
    }

    //call for more data from webservice
    private Flowable<Response<MoviesListStatus>> getMoreList(String apikey, String s, int lastItem) {
        return service.getMoviesList(apikey, s, String.valueOf(lastItem))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //call webservice to get data from net
    private void getMoviesList(Context context, String apikey, String s) {
        Disposable disposable = pagination.onBackpressureDrop()
                .doOnNext(integer -> {
                    requestHasCalled = true;
                    if (integer == 1) {
                        mShimmerViewContainer.startShimmer();
                        mShimmerViewContainer.setVisibility(View.VISIBLE);
                    } else {
                        prb_pagination.setVisibility(View.VISIBLE);
                    }
                })
                .concatMap(new Function<Integer, Publisher<Response<MoviesListStatus>>>() {
                    @Override
                    public Publisher<Response<MoviesListStatus>> apply(Integer nextPage) throws Exception {
                        return getMoreList(apikey, s, nextPage);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Response<MoviesListStatus>>() {
                    @Override
                    public void accept(Response<MoviesListStatus> response) throws Exception {

                        if (response.body().getResponse() != null && response.body().getResponse().equals("True")) {
                            if (response.body().getMoviesListModels() != null && response.body().getMoviesListModels().size() > 0) {
                                if (page == 1) {
                                    mShimmerViewContainer.startShimmer();
                                    mShimmerViewContainer.setVisibility(View.GONE);
                                    setAdapter(response.body().getMoviesListModels(), apikey);
                                } else {
                                    prb_pagination.setVisibility(View.GONE);
                                    notifyAdapter(response.body().getMoviesListModels());
                                }
                            }
                        }

                        requestHasCalled = false;
                        prb_pagination.setVisibility(View.GONE);
                    }
                })
                .doOnError(throwable -> {
                    try {

                        if (page == 1) {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                        } else {
                            prb_pagination.setVisibility(View.GONE);
                        }

                    } catch (Exception ex) {
                        progressAndException.errorHandeling(context, ex);
                    }

                })
                .subscribe();

        compositeDisposable.add(disposable);
        pagination.onNext(page);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        //prevent memory leak
        compositeDisposable.dispose();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public  void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
