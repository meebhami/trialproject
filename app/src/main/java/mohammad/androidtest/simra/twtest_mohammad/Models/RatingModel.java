package mohammad.androidtest.simra.twtest_mohammad.Models;

import com.google.gson.annotations.SerializedName;

public class RatingModel {

    @SerializedName("Source")
    private String Source;
    @SerializedName("Value")
    private String Value;

    public String getSource() {
        return Source;
    }

    public String getValue() {
        return Value;
    }

}
