package mohammad.androidtest.simra.twtest_mohammad.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieDetailsStatus {

    @SerializedName("Response")
    private String Response;


    @SerializedName("Metascore")
    private String Metascore;

    @SerializedName("imdbRating")
    private String imdbRating;


    @SerializedName("imdbVotes")
    private String imdbVotes;


    @SerializedName("imdbID")
    private String imdbID;


    @SerializedName("Type")
    private String Type;


    @SerializedName("DVD")
    private String DVD;


    @SerializedName("BoxOffice")
    private String BoxOffice;


    @SerializedName("Production")
    private String Production;

    @SerializedName("Website")
    private String Website;

    @SerializedName("Title")
    private String Title;

    @SerializedName("Year")
    private String Year;

    @SerializedName("Rated")
    private String Rated;

    @SerializedName("Released")
    private String Released;

    @SerializedName("Runtime")
    private String Runtime;

    @SerializedName("Genre")
    private String Genre;

    @SerializedName("Director")
    private String Director;

    @SerializedName("Writer")
    private String Writer;

    @SerializedName("Actors")
    private String Actors;

    @SerializedName("Plot")
    private String Plot;

    @SerializedName("Language")
    private String Language;

    @SerializedName("Country")
    private String Country;

    @SerializedName("Awards")
    private String Awards;

    @SerializedName("Poster")
    private String Poster;

    @SerializedName("Ratings")
    private ArrayList<RatingModel> ratingModels;

    public String getResponse() {
        return Response;
    }

    public String getMetascore() {
        return Metascore;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getType() {
        return Type;
    }

    public String getDVD() {
        return DVD;
    }

    public String getBoxOffice() {
        return BoxOffice;
    }

    public String getProduction() {
        return Production;
    }

    public String getWebsite() {
        return Website;
    }

    public String getTitle() {
        return Title;
    }

    public String getYear() {
        return Year;
    }

    public String getRated() {
        return Rated;
    }

    public String getReleased() {
        return Released;
    }

    public String getRuntime() {
        return Runtime;
    }

    public String getGenre() {
        return Genre;
    }

    public String getDirector() {
        return Director;
    }

    public String getWriter() {
        return Writer;
    }

    public String getActors() {
        return Actors;
    }

    public String getPlot() {
        return Plot;
    }

    public String getLanguage() {
        return Language;
    }

    public String getCountry() {
        return Country;
    }

    public String getAwards() {
        return Awards;
    }

    public String getPoster() {
        return Poster;
    }

    public ArrayList<RatingModel> getRatingModels() {
        return ratingModels;
    }
}
