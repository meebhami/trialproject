package mohammad.androidtest.simra.twtest_mohammad.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.target.BitmapImageViewTarget;


import java.util.List;

import mohammad.androidtest.simra.twtest_mohammad.Activities.MovieDetailActivity;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListModel;
import mohammad.androidtest.simra.twtest_mohammad.R;

/**
 * Created by mohammad on 11/9/17.
 */

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.myviewholder> {
    private Activity activity=null;
    private LayoutInflater inflator;
    private List<MoviesListModel> moviesListModels;
    private String apiKey;

    public MoviesListAdapter(Context context, List<MoviesListModel> moviesListModels,String apiKey) {

        this.activity = (Activity)context;
        inflator = LayoutInflater.from(context);
        this.moviesListModels=moviesListModels;
        this.apiKey=apiKey;

    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflator.inflate(R.layout.row_movies_list, viewGroup, false);
        myviewholder holder = new myviewholder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(final myviewholder viewHolder, final int position) {
        final MoviesListModel moviesListModel = moviesListModels.get(position);


        if (moviesListModel.getTitle() != null) {
            viewHolder.txt_title.setText(moviesListModel.getTitle());
        } else {
            viewHolder.txt_title.setText("-");
        }

        if (moviesListModel.getType() != null) {
            viewHolder.txt_type.setText(moviesListModel.getType());
        } else {
            viewHolder.txt_type.setText("-");
        }

        if (moviesListModel.getYear() != null) {
            viewHolder.txt_year.setText(moviesListModel.getYear());
        } else {
            viewHolder.txt_year.setText("-");
        }

        if (moviesListModel.getPoster() != null) {
            Glide.with(activity)
                    .load(moviesListModel.getPoster())
                    .into(viewHolder.img_poster);
        }

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MovieDetailActivity.class)
                        .putExtra("ID",moviesListModel.getId())
                        .putExtra("APIKEY",apiKey);
                activity.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moviesListModels.size();
    }


    class myviewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txt_title,txt_year,txt_yearLabel,txt_type;
        ImageView img_poster;
        View view;
        public myviewholder(View itemView) {
            super(itemView);
            view = itemView;
            img_poster= itemView.findViewById(R.id.img_poster);
            txt_title= itemView.findViewById(R.id.txt_title);
            txt_yearLabel= itemView.findViewById(R.id.txt_yearLabel);
            txt_type= itemView.findViewById(R.id.txt_type);
            txt_year= itemView.findViewById(R.id.txt_year);

        }

        @Override
        public void onClick(View v) {
        }
    }



}