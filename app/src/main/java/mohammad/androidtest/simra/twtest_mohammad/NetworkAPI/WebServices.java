package mohammad.androidtest.simra.twtest_mohammad.NetworkAPI;



import java.util.ArrayList;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import mohammad.androidtest.simra.twtest_mohammad.Models.MovieDetailsStatus;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListStatus;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mehdi on 30-Oct-17.
 */

public interface WebServices {



//    @FormUrlEncoded
//    @POST("/api/showAllQuestions_answers")
//    @Headers({"Accept:application/json"})
//    Call<QuestionStatus> getAllQuestionsByAnswers(@Field("category") String category,
//                                                  @Field("search") String keyword, @Field("page") String page,


    @GET("/")
    @Headers({"Accept: application/json"})
    Flowable<Response<MoviesListStatus>> getMoviesList(@Query("apiKey") String apiKey,
                                                       @Query("s") String s,
                                                       @Query("page") String page);

    @GET("/")
    @Headers({"Accept: application/json"})
    Call<MovieDetailsStatus> getMovieDetails(@Query("apiKey") String apiKey,
                                             @Query("i") String s);


}