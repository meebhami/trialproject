package mohammad.androidtest.simra.twtest_mohammad.Models;

import com.google.gson.annotations.SerializedName;

public class MoviesListModel {

    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("imdbID")
    private String id;
    @SerializedName("Type")
    private String type;
    @SerializedName("Poster")
    private String Poster;

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getPoster() {
        return Poster;
    }
}
