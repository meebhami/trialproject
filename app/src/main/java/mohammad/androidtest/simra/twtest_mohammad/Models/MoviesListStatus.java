package mohammad.androidtest.simra.twtest_mohammad.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MoviesListStatus {

    @SerializedName("Response")
    private String Response;

    @SerializedName("totalResults")
    private String totalResults;


    @SerializedName("Search")
    private ArrayList<MoviesListModel> moviesListModels;


    public String getResponse() {
        return Response;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public ArrayList<MoviesListModel> getMoviesListModels() {
        return moviesListModels;
    }
}
