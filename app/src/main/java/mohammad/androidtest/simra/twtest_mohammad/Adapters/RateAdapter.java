package mohammad.androidtest.simra.twtest_mohammad.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import mohammad.androidtest.simra.twtest_mohammad.Activities.MovieDetailActivity;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListModel;
import mohammad.androidtest.simra.twtest_mohammad.Models.RatingModel;
import mohammad.androidtest.simra.twtest_mohammad.R;

/**
 * Created by mohammad on 11/9/17.
 */

public class RateAdapter extends RecyclerView.Adapter<RateAdapter.myviewholder> {
    private Activity activity=null;
    private LayoutInflater inflator;
    private List<RatingModel> ratingModels;
    private String apiKey;

    public RateAdapter(Context context, List<RatingModel> ratingModels) {

        this.activity = (Activity)context;
        inflator = LayoutInflater.from(context);
        this.ratingModels=ratingModels;


    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflator.inflate(R.layout.row_rate, viewGroup, false);
        myviewholder holder = new myviewholder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(final myviewholder viewHolder, final int position) {
        final RatingModel ratingModel= ratingModels.get(position);

        if (ratingModel.getSource() != null) {
            viewHolder.txt_source.setText(ratingModel.getSource());
        } else {
            viewHolder.txt_source.setText("-");
        }


        if (ratingModel.getValue() != null) {
            viewHolder.prb_rate.setVisibility(View.VISIBLE);
            viewHolder.txt_value.setText(ratingModel.getValue());

            if(ratingModel.getValue().contains("%")){

                viewHolder.prb_rate.setMax(100);
                int rate=Integer.parseInt(ratingModel.getValue().substring(0, ratingModel.getValue().length() - 1));
                viewHolder.prb_rate.setProgress(rate);

            }else if(ratingModel.getValue().contains("/")){

                float rate= Float.parseFloat(ratingModel.getValue().split("/")[0]);
                float percent= Float.parseFloat(ratingModel.getValue().split("/")[1]);

                viewHolder.prb_rate.setMax((int) percent);
                viewHolder.prb_rate.setProgress((int)rate);
            }


        } else {
            viewHolder.prb_rate.setVisibility(View.GONE);
        }




        }

    @Override
    public int getItemCount() {
        return ratingModels.size();
    }


    class myviewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txt_source,txt_value;
        ProgressBar prb_rate;

        public myviewholder(View itemView) {
            super(itemView);


            txt_source= itemView.findViewById(R.id.txt_source);
            prb_rate= itemView.findViewById(R.id.prb_rate);
            txt_value= itemView.findViewById(R.id.txt_value);


        }

        @Override
        public void onClick(View v) {
        }
    }



}