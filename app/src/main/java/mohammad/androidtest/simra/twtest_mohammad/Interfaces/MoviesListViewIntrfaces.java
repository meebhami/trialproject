package mohammad.androidtest.simra.twtest_mohammad.Interfaces;

import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListStatus;

public interface MoviesListViewIntrfaces {

    void showToast(String s);
    void displayMovies(MoviesListStatus moviesListStatus);
    void displayError(String s);

}
