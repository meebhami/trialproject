package mohammad.androidtest.simra.twtest_mohammad.Activities;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

import mohammad.androidtest.simra.twtest_mohammad.Adapters.MoviesListAdapter;
import mohammad.androidtest.simra.twtest_mohammad.Adapters.RateAdapter;
import mohammad.androidtest.simra.twtest_mohammad.Helper.CheckNet;
import mohammad.androidtest.simra.twtest_mohammad.Helper.NetworkChecker;
import mohammad.androidtest.simra.twtest_mohammad.Helper.ProgressAndException;
import mohammad.androidtest.simra.twtest_mohammad.Models.MovieDetailsStatus;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListModel;
import mohammad.androidtest.simra.twtest_mohammad.Models.MoviesListStatus;
import mohammad.androidtest.simra.twtest_mohammad.Models.RatingModel;
import mohammad.androidtest.simra.twtest_mohammad.NetworkAPI.RetrofitInstance;
import mohammad.androidtest.simra.twtest_mohammad.NetworkAPI.WebServices;
import mohammad.androidtest.simra.twtest_mohammad.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailActivity extends AppCompatActivity {




    ProgressAndException progressAndException;
    String id="",apiKey="";
    Intent intent;
    TextView txt_title,txt_titleTop,txt_year,txt_realesed,txt_director,txt_type, txt_actors,txt_plots;
    ImageView img_poster,img_back;
    RecyclerView rec_rate;
    NestedScrollView nes_main;



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // do nothing, just override
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_detail);



        progressAndException = new ProgressAndException();


        intent =getIntent();
        if (intent.hasExtra("ID")) {
            id =  intent.getStringExtra("ID");
            apiKey =  intent.getStringExtra("APIKEY");
        }

        initial();
        event();
        getMovieDetails(MovieDetailActivity.this, apiKey,id);
    }

    private void initial(){

        nes_main = findViewById(R.id.nes_main);
        img_poster= findViewById(R.id.img_poster);
        img_back= findViewById(R.id.img_back);
        txt_title= findViewById(R.id.txt_title);
        txt_titleTop= findViewById(R.id.txt_titleTop);
        txt_year= findViewById(R.id.txt_year);
        txt_realesed= findViewById(R.id.txt_realesed);
        txt_director= findViewById(R.id.txt_director);
        txt_type= findViewById(R.id.txt_type);


        txt_actors= findViewById(R.id.txt_actors);
        txt_plots= findViewById(R.id.txt_plots);
        rec_rate= findViewById(R.id.rec_rate);



    }

    private void event(){

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieDetailActivity.super.onBackPressed();
            }
        });
    }

    private void getMovieDetails(final Context context, String apiKey , String id) {
        if (NetworkChecker.isOnline(this)) {
            progressAndException.startProgress(context);
            WebServices service = RetrofitInstance.getRetrofitInstance(context).create(WebServices.class);
            Call<MovieDetailsStatus> call = service.getMovieDetails(apiKey, id);
            call.enqueue(new Callback<MovieDetailsStatus>() {
                @Override
                public void onResponse(@NonNull Call<MovieDetailsStatus> call, @NonNull Response<MovieDetailsStatus> response) {
                    if (response.body().getResponse() != null && response.body().getResponse().equals("True")) {

                            setData(context,response.body());

                    }
                    progressAndException.endProgress(context);
                }

                @Override
                public void onFailure(@NonNull Call<MovieDetailsStatus> call, Throwable t) {
                    progressAndException.endProgress(context);
                    progressAndException.errorHandeling(context, t);

                }
            });
        } else {
            new CheckNet().ShowDialog(this);
        }
    }

    private void setData(Context context,MovieDetailsStatus movieDetailsStatus){
        nes_main.setVisibility(View.VISIBLE);
        if (movieDetailsStatus.getPoster() != null) {
            Glide.with(MovieDetailActivity.this)
                    .load(movieDetailsStatus.getPoster())
                    .into(img_poster);
        }

        if (movieDetailsStatus.getTitle() != null) {
            txt_title.setText(movieDetailsStatus.getTitle());
            txt_titleTop.setText(movieDetailsStatus.getTitle());
        } else {
            txt_title.setText("-");
            txt_titleTop.setText("");
        }



        if (movieDetailsStatus.getDirector() != null) {
            txt_director.setText(" Director : "+movieDetailsStatus.getDirector());
        } else {
            txt_director.setText(" Director : "+ "-");
        }

        if (movieDetailsStatus.getReleased() != null) {
            txt_realesed.setText(" Released : "+movieDetailsStatus.getReleased());
        } else {
            txt_realesed.setText(" Released : "+"-");
        }

        if (movieDetailsStatus.getType() != null) {
            txt_type.setText(" Movie : "+movieDetailsStatus.getType());
        } else {
            txt_type.setText(" Movie : "+"-");
        }

        if (movieDetailsStatus.getYear() != null) {
            txt_year.setText(" Year : "+movieDetailsStatus.getYear());
        } else {
            txt_year.setText(" Year : "+"-");
        }

        if (movieDetailsStatus.getActors() != null) {
            txt_actors.setText(movieDetailsStatus.getActors());
        } else {
            txt_actors.setText("-");
        }

        if (movieDetailsStatus.getPlot() != null) {
            txt_plots.setText(movieDetailsStatus.getPlot());
        } else {
            txt_plots.setText("-");
        }

        if (movieDetailsStatus.getRatingModels() != null && movieDetailsStatus.getRatingModels().size() > 0) {
            rec_rate.setVisibility(View.VISIBLE);
            setToAdapter(context,movieDetailsStatus.getRatingModels());
        } else {
            rec_rate.setVisibility(View.GONE);
        }




    }

    private void setToAdapter(Context context,ArrayList<RatingModel> ratingModels){

        RateAdapter rateAdapter= new RateAdapter(context,ratingModels);
        rec_rate.setAdapter(rateAdapter);
        LinearLayoutManager linearLayoutManagerH = new LinearLayoutManager(context);
        linearLayoutManagerH.setOrientation(LinearLayoutManager.VERTICAL);
        rec_rate.setLayoutManager(linearLayoutManagerH);
        rec_rate.setNestedScrollingEnabled(false);
    }
}
