package mohammad.androidtest.simra.twtest_mohammad.Helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;


import java.util.Timer;
import java.util.TimerTask;

import mohammad.androidtest.simra.twtest_mohammad.R;

/**
 * Created by mohammad on 2/27/18.
 */

public class CheckNet {


    public void ShowDialog(final Context context){
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_no_net);

        final ConstraintLayout con_net=dialog.findViewById(R.id.con_net);


        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (NetworkChecker.isOnline(context)) {
                    dialog.dismiss();
                }
            }
        }, 0, 2000);//put here time 1000 milliseconds=1 second


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.99);
        lp.height= (int) (context.getResources().getDisplayMetrics().heightPixels * 0.99);


        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);


        dialog.show();

    }
}
